import numpy as np
from mnist_loader import load_data_wrapper
import matplotlib.pyplot as plt
import time


class nn1(object):
    def __init__(self, sizes):
        self.num_of_layers = len(sizes)
        self.sizes = sizes
        self.biases = []
        self.error = []
        for y in sizes[1:]:
            self.biases.append(np.random.randn(y, 1))
        self.weights = []
        for x, y in zip(sizes[:-1], sizes[1:]):
            self.weights.append(np.random.randn(y, x))

    def gradient_decent(self, training_data, test_data, epochs, mini_batch_size, learning_rate):
        training_data = list(training_data)
        training_data_size = len(training_data)

        test_data = list(test_data)
        test_data_size = len(test_data)

        num = []

        for i in range(epochs):
            start = time.time()
            np.random.shuffle(training_data)
            mini_batches = [training_data[j: j + mini_batch_size] for j in range(0, training_data_size, mini_batch_size)]
            for mini_batch in mini_batches:
                self.update(mini_batch, learning_rate)
            evaluate = self.evaluate(test_data)
            print("Epoch {0}/{1}: {2} / {3} -------- time: {4}s".format(i + 1, epochs, evaluate, test_data_size, int(time.time() - start)))
            num.append(i)
            self.error.append(test_data_size - evaluate)

        plt.scatter(num, self.error)
        plt.savefig('error')

    def update(self, mini_batch, learning_rate):
        biases = [np.zeros(b.shape) for b in self.biases]
        weights = [np.zeros(w.shape) for w in self.weights]

        for x, y in mini_batch:
            delta_bias, delta_weight = self.backprop(x, y)
            biases = [b + db for b, db in zip(biases, delta_bias)]
            weights = [w + dw for w, dw in zip(weights, delta_weight)]
        self.weights = [wi - (learning_rate / len(mini_batch)) * w for wi, w in zip(self.weights, weights)]
        self.biases = [bi - (learning_rate / len(mini_batch)) * b for bi, b in zip(self.biases, biases)]

    def backprop(self, x, y):
        biases = [np.zeros(b.shape) for b in self.biases]
        weights = [np.zeros(w.shape) for w in self.weights]
        activation = x
        activations = [x]
        zs = []
        for b, w in zip(self.biases, self.weights):
            z = sum(np.dot(w, activation)) + b
            zs.append(z)
            activation = sigmoid(z)
            activations.append(activation)

        error = (activations[-1] - y) * sigmoid_prime(zs[-1])
        biases[-1] = error
        weights[-1] = np.dot(error, activations[-2].transpose())
        for l in range(2, self.num_of_layers):
            error = np.dot(self.weights[-l + 1].transpose(), error) * sigmoid_prime(zs[-l])
            biases[-l] = error
            weights[-l] = np.dot(error, activations[-l - 1].transpose())
        return biases, weights

    def feed_forward(self, a):
        for b, w in zip(self.biases, self.weights):
            a = sigmoid(np.dot(w, a)+b)
        return a

    def evaluate(self, test_data):
        test_results = [(np.argmax(self.feed_forward(x)), y) for (x, y) in test_data]
        return sum(int(x == y) for (x, y) in test_results)


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def sigmoid_prime(x):
    return sigmoid(x) * (1 - sigmoid(x))

net = nn1([784, 15, 10])
training, valid, test = load_data_wrapper()
net.gradient_decent(training, test, 30, 10, 3)
print(net.evaluate(valid))

